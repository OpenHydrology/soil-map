# Soil map

Python program for raster soil map generation from sand/clay/silt granulation. Source raster layers with 250 m resolution can be found at [SoilGrids project](https://soilgrids.org) and [downloaded directly](https://files.isric.org/soilgrids/data/recent/) from the server.

<img src="https://gitlab.com/OpenHydrology/soil-map/raw/master/soil-map/plot.png" alt="ternary plot" width="600" />


## Getting Started


### Dependencies

Basic usage requires only `rasterio` module. You can install it with `pip` or by creating conda environment from YAML file.

```
conda env create -f soil-map.yml
```

Ternary plots can be made with `python-ternary` module. 

```
conda config --add channels conda-forge
conda install python-ternary
```


### Basic usage

Run program with sample 2nd level sand/clay/silt granulation layer. Raster layer were clipped to the area of Poland and reprojected to EPSG:3035.

```
python raster.py
```

Sample layer and resulting `SOIL_sl2.tif` raster layer can be displayed in QGIS.

### Updating dependencies

```
conda env export > soil-map.yml
```


## Authors and citation

Please cite as follows:

* Baziak, B, Gądek, W., Szczepanek, R., 2019, GLEMOK - novel method for catchment moisture determination 
using high-resolution soil map, [Applied Ecology and Environmental Research](http://www.aloki.hu/), DOI: [http://dx.doi.org/10.15666/aeer/1706_1266712681](http://dx.doi.org/10.15666/aeer/1706_1266712681)


## License

Copyright (C) 2019  Robert Szczepanek

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.



