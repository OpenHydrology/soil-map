# -*- coding: utf-8 -*-

__author__      = "Robert Szczepanek"
__email__       = "robert@szczepanek.pl"

import rasterio


path = '../data/'
f_end = '-3035.tif'
f = ('SND', 'CLY', 'SLT')

CATS = {'heavy clay': 1,
        'light clay': 2,
        'silty clay': 3,
        'silty clayloam': 4,
        'clayloam': 5,
        'sandy clayloam': 6,
        'silty loam': 7,
        'loam': 8,
        'sandy loam': 9,
        'loamy sand': 10,
        'sand': 11}


def cat(clay, silt, sand):
    """Assign category to combination of clay, silt and sand
    """

    # convert uint8 from raster to int
    clay = int(clay)
    silt = int(silt)
    sand = int(sand)

    # no data
    if clay == 255 or silt == 255 or sand == 255:
        return 255

    if clay > 65:
        c = 'heavy clay'
    elif clay > 40:
        if silt < 40:
            c = 'light clay'
        else:
            c = 'silty clay'
    elif clay > 30:
        if sand < 20:
            c = 'silty clayloam'
        elif sand < 45:
            c = 'clayloam'
        else:
            c = 'sandy clayloam'
    elif silt > 50:
        c = 'silty loam'
    elif clay > 20 and silt < 25:
        c = 'sandy clayloam'
    elif clay > 10 and sand < 55:
        c = 'loam'
    elif clay - sand > -70:
        c = 'sandy loam'
    elif clay - 2 * sand > -170:
        c = 'loamy sand'
    else:
        c = 'sand'

    return CATS[c]


def test_cat():
    from random import randint

    for i in range(20):
        sand = randint(60, 100)
        clay = randint(0, 100 - sand)
        silt = 100 - clay - sand
        print(clay, silt, sand, cat(clay, silt, sand)[0])


def info(path, f_name):
    with rasterio.open(path + f_name) as src:
        print(src.width, src.height)
        print(src.crs)
        print(src.transform)
        print(src.count)
        print(src.indexes)
        print(src)
        dane = src.read(1)
        print(dane)


def mix_rasters(level=None):
    """Create granulation map from components
    """

    x = y = 1500
    #with rasterio.open(path + f[0] + f_end, 'r') as src_snd:
    f_name = '{}{}_sl{}{}'.format(path, f[0], level, f_end)
    with rasterio.open(f_name, 'r') as src_snd:
        snd = src_snd.read(1)
        print(snd.shape)

        f_name = '{}{}_sl{}{}'.format(path, f[1], level, f_end)
        with rasterio.open(f_name, 'r') as src_cly:
            cly = src_cly.read(1)

            f_name = '{}{}_sl{}{}'.format(path, f[2], level, f_end)
            with rasterio.open(f_name, 'r') as src_slt:
                slt = src_slt.read(1)

                print('sand: ', snd[x, y])
                print('clay: ', cly[x, y])
                print('silt: ', slt[x, y])
                print(cat(cly[x,y], slt[x,y], snd[x,y]))

                out_image = cly
                for x in range(cly.shape[0]):
                    for y in range(cly.shape[1]):
                        out_image[x,y] = cat(cly[x,y], slt[x,y], snd[x,y])


                out_meta = src_snd.meta.copy()
                out_meta.update({"driver": "GTiff", "compress": 'lzw'})
#                print(out_meta)
#                print(out_image.shape)
                f_name = '{}SOIL_sl{}.tif'.format(path, level)
                with rasterio.open(f_name, "w", **out_meta) as dest:
                    dest.write(out_image, 1)




def plot(path, f_name):
    from rasterio.plot import show_hist

    with rasterio.open(path + f_name) as src:
        show_hist(
            src, bins=50, lw=0.0, stacked=False, alpha=0.3,
            histtype='stepfilled', title="Histogram")


def main():
#    info(path, f[0] + file_name)
#    plot(path, f[0] + file_name)
#    test_cat()
    mix_rasters(level=2)


if __name__ == '__main__':
    main()
    