# -*- coding: utf-8 -*-

__author__      = "Robert Szczepanek"
__email__       = "robert@szczepanek.pl"

# Based on python-ternary module
# https://github.com/marcharper/python-ternary

import ternary


def fractions():
    data = [((0, 10), (15, 0)), # sand
            ((0, 15), (30, 0)), # loadmy sand
            ((0, 20), (25, 20)), # sandy loam
            ((25, 20), (40, 10)),
            ((40, 10), (50, 10)),
            ((50, 0), (50, 30)),
            ((25, 20), (25, 30)), # loam
            ((25, 30), (70, 30)), # silty loam
            ((25, 30), (15, 40)), # sandy clayloam
            ((0, 40), (60, 40)),
            ((50, 30), (40, 40)), # clayloam / silty clayloam
            ((40, 40), (40, 60)), # silty clay
            ((0, 65), (35, 65)) # light clay / heavy clay
            ]
    text = [(3, 1, "sand"),
            (9, 3, "loamy \n   sand"),
            (20, 10, "sandy loam"),
            (70, 10, "silty loam"),
            (45, 20, "loam"),
            (45, 30, "clayloam"),
            (20, 25, "  sandy \nclayloam"),
            (67, 28, "  silty \nclayloam"),
            (67, 38, "silty \n clay"),
            (40, 45, "light clay"),
            (45, 65, "heavy \n clay")
            ]
    
    return data, text


## Boundary and Gridlines
scale = 100
figure, tax = ternary.figure(scale=scale)
figure.set_size_inches(10, 10)
figure.set_dpi(600)

# Draw Boundary and Gridlines
tax.boundary(linewidth=1.0)
tax.gridlines(color="black", multiple=10)
# tax.gridlines(color="blue", multiple=2, linewidth=0.5)

# Set Axis labels and Title
fontsize = 12
# tax.set_title("cccc", fontsize=fontsize)
tax.left_axis_label("Clay [%]", fontsize=fontsize)
tax.right_axis_label("Silt [%]", fontsize=fontsize)
tax.bottom_axis_label("Sand [%]", fontsize=fontsize)

# Set ticks
# permutation='201'
tax.ticks(axis='lrb', linewidth=1, clockwise=True, multiple=10, fontsize=10)

# Remove default Matplotlib Axes
tax.clear_matplotlib_ticks()

# remove frame
ax = tax.get_axes() # or just tax.ax
ax.axis('off')

data, text = fractions()
for line in data:
    tax.line(line[0], line[1], linewidth=2., color='blue')

for t in text:
    ax.text(t[0], t[1], t[2], fontsize=15, color='blue')
    print(t)

sample = False
if sample:
    tax.horizontal_line(16)
    tax.left_parallel_line(10, linewidth=2., color='red', linestyle="--")
    tax.right_parallel_line(20, linewidth=3., color='blue')
    p1 = (12,8,10)
    p2 = (2, 26, 2)
    tax.line(p1, p2, linewidth=3., marker='s', color='green', linestyle=":")
    
    # points
    p = [(20, 10, 0)]
    p.append((0, 30, 40))
    p.append((30, 50, 0))
    #p.append((30, 30, 30))
    print(p)

    tax.scatter(p, marker='o', color='red', label="ddd")


tax.legend()

tax.savefig('plot.png')
#ternary.plt.show()
